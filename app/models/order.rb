class Order < ActiveRecord::Base
  attr_accessible :customer_id, :price
  belongs_to :customer
  has_many :order_items, dependent: :destroy
end
