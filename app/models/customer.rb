class Customer < ActiveRecord::Base
  attr_accessible :age, :name, :terms_of_service, :name_confirmation
  has_many :orders, dependent: :destroy
  validates :terms_of_service, acceptance: true
  validates :name, length: { maximum: 6, too_long: "%{count} characters is the maximum allowed" }
  validates :name, confirmation: true
end
