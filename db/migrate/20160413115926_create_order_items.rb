class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :customer_id
      t.string :item_name

      t.timestamps
    end
  end
end
